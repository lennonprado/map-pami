// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBwioDap9xs_kHl9OF80tL72aVLqX3kv-U",
    authDomain: "voluntariado-covid19.firebaseapp.com",
    databaseURL: "https://voluntariado-covid19.firebaseio.com",
    projectId: "voluntariado-covid19",
    storageBucket: "voluntariado-covid19.appspot.com",
    messagingSenderId: "189862711929",
    appId: "1:189862711929:web:aeccc8a00716763a8f3aa8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
