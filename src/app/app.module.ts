
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
 
import { environment } from '../environments/environment';

import { VoluntarioComponent } from './components/voluntario/voluntario.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LoginComponent } from './components/login/login.component';
import { MayoresComponent } from './components/mayores/mayores.component';
import { EditvoluntarioComponent } from './components/editvoluntario/editvoluntario.component';
import { DistanciasComponent } from './components/distancias/distancias.component';
import { MapaComponent } from './components/mapa/mapa.component';
import { EditmayoresComponent } from './components/editmayores/editmayores.component';
import { ListvoluntariosComponent } from './components/listvoluntarios/listvoluntarios.component';
import { ListbeneficiariosComponent } from './components/listbeneficiarios/listbeneficiarios.component';
import { AsignacionesComponent } from './components/asignaciones/asignaciones.component';

import { FirebaseService } from './services/firebase.service';
import { ListadoasignadosComponent } from './components/listadoasignados/listadoasignados.component';


@NgModule({
  declarations: [
    AppComponent,
    VoluntarioComponent,
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    LoginComponent,
    MayoresComponent,
    EditvoluntarioComponent,
    DistanciasComponent,
    MapaComponent,
    EditmayoresComponent,
    ListvoluntariosComponent,
    ListbeneficiariosComponent,
    AsignacionesComponent,
    ListadoasignadosComponent,
  ],
  imports: [    
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
