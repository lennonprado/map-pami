import { FirebaseService } from '../../services/firebase.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor( 
    private fs: FirebaseService, 
    private afAuth: AngularFireAuth,
    private router: Router) {}

  ngOnInit(): void {
  }

  onSubmit(f: NgForm) {
    
     let password : string =   f.value.password.toString();
     let email : string =   f.value.email.toString();

     
    this.afAuth.signInWithEmailAndPassword(email, password ).then((user) => {
         //this.router.navigate(['/todos']);
         //console.log(user.user.email);


        
         this.fs.getCiudad(user.user.email).subscribe(
           data => { 
            this.fs.setCity(data.payload.data()); 
            this.fs.setUser(user);        
            this.router.navigate(['/mapa']);

           },
           error => { alert(JSON.stringify(error)); }
         ); 
      
      

        
       }).catch(response => {
        console.log(response);
    });
    
  
  }


}
