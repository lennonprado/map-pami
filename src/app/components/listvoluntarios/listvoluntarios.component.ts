import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listvoluntario',
  templateUrl: './listvoluntarios.component.html',
  styleUrls: ['./listvoluntarios.component.scss']
})
export class ListvoluntariosComponent implements OnInit {

  voluntarios : any[] = [];
  
  constructor(private fs: FirebaseService  ) { }

  ngOnInit(): void {
    this.getVoluntarios();
  }

  getVoluntarios(){
    return this.fs.getVoluntarios().subscribe(
      response => {
        
          this.voluntarios = [];
          response.forEach(doc => { 
            let element = doc.data();
            element.id = doc.id; 
            this.voluntarios.push(element);          
          });
        
      },
      error => {console.log(error);
      }
    );
  }

  deleteVoluntario(item){
    if(confirm("Seguro quiere eliminar a " + item.nombre + " " + item.apellido )) {
      this.fs.deleteVoluntario(item.id);
    }
  }

}
