import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListvoluntariosComponent } from './listvoluntarios.component';

describe('ListvoluntariosComponent', () => {
  let component: ListvoluntariosComponent;
  let fixture: ComponentFixture<ListvoluntariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListvoluntariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListvoluntariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
