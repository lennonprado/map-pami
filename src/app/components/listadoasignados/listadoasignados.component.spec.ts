import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoasignadosComponent } from './listadoasignados.component';

describe('ListadoasignadosComponent', () => {
  let component: ListadoasignadosComponent;
  let fixture: ComponentFixture<ListadoasignadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoasignadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoasignadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
