import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import * as Leaflet from 'leaflet';

declare const L: any; 

@Component({
  selector: 'app-mayores',
  templateUrl: './mayores.component.html',
  styleUrls: ['./mayores.component.scss']
})
export class MayoresComponent implements OnInit {

  latitude: any = -37.32872186977888;
  longitude: any = -59.1369104424321;
  zoom = 15;
  selected = false;
  submited = false;
  map: any;
  estado: any;
  public marker: any = undefined;
  voluntarioSelected : any;

  iconMayor = L.icon({
    iconUrl: 'assets/img/mayor.png',
    shadowUrl: 'assets/img/shadow.png',
    iconSize: [35, 50],
    shadowSize:   [50, 50],
    iconAnchor: [18, 50],
    shadowAnchor: [18, 50],
    popupAnchor: [0, 0],
  });


  mapaXYZ = 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';
  
  constructor( private fs: FirebaseService) {}
  @ViewChild('signupForm') form: NgForm;

  ngOnInit() {
    this.drawMap();
    this.setCurrentLocation();

  }

  onSubmit(f: NgForm) {
    this.map.removeLayer(this.marker);

    this.marker = undefined;
    if(this.selected){ 
    f.value.latitude = this.latitude;
    f.value.longitude = this.longitude;
    f.value.asociado = [];

    this.fs.addMayor(f.value).then(() => {
      window.scroll(0,0);
      f.reset();
      this.submited = true;
      this.map.removeLayer(this.marker);
    }, (error) => {
      console.error(error);
      
      alert("Hubo un error al guardar los datos en la base de datos, asegurese de tener internet.");
    });

    }
    else{
      alert("Seleccione su localización en el mapa.");
    }
  }

  drawMap(): void {
    this.map = Leaflet.map('map').setView([this.latitude, this.longitude], this.zoom);
    Leaflet.tileLayer(this.mapaXYZ, {attribution: 'Extension UNICEN',maxZoom: 17}).addTo(this.map);
    // Add marker on click
    this.map.on('click', (e) =>{ this.addmarker(e); });
  }
  
  addmarker(e){
    this.selected = true;
    this.latitude = e.latlng.lat;
    this.longitude = e.latlng.lng;
    if(this.marker != undefined) 
    this.map.removeLayer(this.marker);
    let marker = L.marker([this.latitude,this.longitude],{ draggable: true, icon : this.iconMayor}).addTo(this.map);    
    this.marker = marker;  
    this.map.setView([e.latlng.lat,e.latlng.lng]);
  }

  addmarker2(lat,lon){
    this.selected = true;
    this.latitude = lat;
    this.longitude = lon;
    if(this.marker != undefined) 
    this.map.removeLayer(this.marker);
    let marker = L.marker([this.latitude,this.longitude],{ draggable: true, icon : this.iconMayor}).addTo(this.map);    
    this.marker = marker;      
    this.map.setView([lat,lon]);
    console.log("LAT: " + this.latitude + "LON: " + this.latitude );
  }

  public buscarDireccion(e){

//    console.log(this.form.value.direccion);

    this.fs.getPuntosGis(this.form.value.direccion + ', ' + this.form.value.ciudad).subscribe(
      data => {
        if(data[0] != undefined ){
          let e;
          this.addmarker2(data[0].lat, data[0].lon);
          
        }
      
      },
      error => { console.log(error)}
    );
    
  }

  onSelectVoluntario(voluntario) {
   this.voluntarioSelected = voluntario
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      
      
      navigator.geolocation.getCurrentPosition((position) => {
        console.log(position);
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.map.setView([this.latitude, this.longitude], this.zoom);        
      });
    }
    
  }

}
