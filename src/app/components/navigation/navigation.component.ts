import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  statusMenu: boolean = false;

  constructor(public fs : FirebaseService, private router: Router) { }

  ngOnInit(): void {
  }

  loggedIn(){
    return this.fs.loggedIn();
  }
  clickEventMenu(){
    this.statusMenu = !this.statusMenu;           
  }

  logout(){
    this.fs.logout();
  }
}
