import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditvoluntarioComponent } from './editvoluntario.component';

describe('EditvoluntarioComponent', () => {
  let component: EditvoluntarioComponent;
  let fixture: ComponentFixture<EditvoluntarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditvoluntarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditvoluntarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
