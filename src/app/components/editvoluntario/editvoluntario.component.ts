import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import * as Leaflet from 'leaflet';
import { ActivatedRoute, Router } from '@angular/router';

declare const L: any; 

@Component({
  selector: 'app-editvoluntario',
  templateUrl: './editvoluntario.component.html',
  styleUrls: ['./editvoluntario.component.scss']
})
export class EditvoluntarioComponent implements OnInit {

  id;
  voluntario : any = {};
  zoom = 15;
  map: any;
  marker: any = undefined;

  iconVoluntario = L.icon({
    iconUrl: 'assets/img/voluntario.png',
    shadowUrl: 'assets/img/shadow.png',
    iconSize: [35, 50],
    shadowSize:   [50, 50],
    iconAnchor: [18, 50],
    shadowAnchor: [18, 50],
    popupAnchor: [0, 0],
  });


  mapaXYZ = 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';
  
  constructor( private fs: FirebaseService, private route : ActivatedRoute, private router: Router,) {}
  
  @ViewChild('signupForm') form: NgForm;

  ngOnInit() {

    this.route.params.subscribe(params => {
      
      this.id = params['id'];

      this.fs.getVoluntario(this.id).subscribe(
        (doc) => { 
          this.voluntario = doc.payload.data();     
          this.drawMap(); 
          this.addmarker2(this.voluntario.latitude, this.voluntario.longitude ) 
        },
        (error) => {}
      );

    });
    
  }

  test(){
    
    
  }

  onSubmit(f: NgForm) {

    this.fs.updateVoluntario(this.id, this.voluntario);
    this.router.navigate(['/listadovoluntarios'])
  }

  drawMap(): void {
    this.map = Leaflet.map('map').setView([this.voluntario.latitude, this.voluntario.longitude], this.zoom);
    Leaflet.tileLayer(this.mapaXYZ, {attribution: 'Extension UNICEN',maxZoom: 20}).addTo(this.map);
    // Add marker on click
    this.map.on('click', (e) =>{ this.addmarker(e); });
  }
  
  addmarker(e){
    this.addmarker2(e.latlng.lat,e.latlng.lng);    
  }

  addmarker2(lat,lon){
    this.voluntario.latitude = lat;
    this.voluntario.longitude = lon;
    if(this.marker != undefined) 
    this.map.removeLayer(this.marker);
    let marker = L.marker([this.voluntario.latitude,this.voluntario.longitude],{ draggable: true, icon : this.iconVoluntario}).addTo(this.map);    
    this.marker = marker;      
    this.map.setView([lat,lon]);    
  }

  public buscarDireccion(e){
    this.fs.getPuntosGis(this.form.value.direccion + ', ' + this.form.value.ciudad).subscribe(
      data => {
        if(data[0] != undefined ){
          this.addmarker2(data[0].lat, data[0].lon);
        }
      },
      error => { console.log(error)}
    );
    
  }
}
