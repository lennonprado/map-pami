import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import * as Leaflet from 'leaflet';
import { ActivatedRoute, Router } from '@angular/router';

declare const L: any; 


@Component({
  selector: 'app-mayores',
  templateUrl: './editmayores.component.html',
  styleUrls: ['./editmayores.component.scss']
})
export class EditmayoresComponent implements OnInit {

  id;
  beneficiario : any = {};
  voluntarios : any[] = [];
  zoom = 15;
  map: any;
  marker: any = undefined;

  iconMayor = L.icon({
    iconUrl: 'assets/img/mayor.png',
    shadowUrl: 'assets/img/shadow.png',
    iconSize: [35, 50],
    shadowSize:   [50, 50],
    iconAnchor: [18, 50],
    shadowAnchor: [18, 50],
    popupAnchor: [0, 0],
  });


  mapaXYZ = 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';
  
  constructor( private fs: FirebaseService, private route : ActivatedRoute, private router: Router,) {}
  
  @ViewChild('signupForm') form: NgForm;

  ngOnInit() {

    this.route.params.subscribe(params => {
      
      this.id = params['id'];

      this.fs.getBeneficiario(this.id).subscribe(
        (doc) => { 
          this.beneficiario = doc.payload.data();
          this.voluntarios = this.beneficiario.asociado;     
          this.drawMap(); 
          this.addmarker2(this.beneficiario.latitude, this.beneficiario.longitude ) 
        },
        (error) => {}
      );

    });
    
  }

  deleteVoluntario(id){

    for( let i = 0; i < this.voluntarios.length; i++){ 
      if ( this.voluntarios[i] === id) { 
        this.voluntarios.splice(i, 1); 
      }
    }

  }

  onSubmit(f: NgForm) {
    this.beneficiario.asociado = this.voluntarios;
    this.fs.updateMayor(this.id, this.beneficiario);
    this.router.navigate(['/listadobeneficiarios'])
  }

  drawMap(): void {
    this.map = Leaflet.map('map').setView([this.beneficiario.latitude, this.beneficiario.longitude], this.zoom);
    Leaflet.tileLayer(this.mapaXYZ, {attribution: 'Extension UNICEN',maxZoom: 20}).addTo(this.map);
    // Add marker on click
    this.map.on('click', (e) =>{ this.addmarker(e); });
  }
  
  addmarker(e){
    this.addmarker2(e.latlng.lat,e.latlng.lng);    
  }

  addmarker2(lat,lon){
    this.beneficiario.latitude = lat;
    this.beneficiario.longitude = lon;
    if(this.marker != undefined) 
    this.map.removeLayer(this.marker);
    let marker = L.marker([this.beneficiario.latitude,this.beneficiario.longitude],{ draggable: true, icon : this.iconMayor}).addTo(this.map);    
    this.marker = marker;      
    this.map.setView([lat,lon]);    
  }

  public buscarDireccion(e){
    this.fs.getPuntosGis(this.form.value.direccion + ', ' + this.form.value.ciudad).subscribe(
      data => {
        if(data[0] != undefined ){
          this.addmarker2(data[0].lat, data[0].lon);
        }
      },
      error => { console.log(error)}
    );
    
  }

}
