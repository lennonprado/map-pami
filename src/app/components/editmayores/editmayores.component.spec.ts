import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditmayoresComponent } from './editmayores.component';

describe('EditmayoresComponent', () => {
  let component: EditmayoresComponent;
  let fixture: ComponentFixture<EditmayoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditmayoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditmayoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
