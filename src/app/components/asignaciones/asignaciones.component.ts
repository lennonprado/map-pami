import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-asignaciones',
  templateUrl: './asignaciones.component.html',
  styleUrls: ['./asignaciones.component.scss']
})
export class AsignacionesComponent implements OnInit {
  
  id;
  beneficiario : any = {};
  voluntario : any;
  loaded : boolean = false;
  constructor( private fs: FirebaseService, private route : ActivatedRoute, private router: Router,) {}

  ngOnInit() {

    this.route.params.subscribe(params => {
      
      this.id = params['id'];

      this.fs.getBeneficiario(this.id).subscribe(
        (doc) => { 
          this.beneficiario = doc.payload.data();   
          this.loaded = true;           
        },
        (error) => {}
      );

    });
    
  }

  onSelectVoluntario(voluntario) {
    this.voluntario = voluntario
   }

   onSubmit(){
    if(confirm("Desea asignar a " + this.voluntario.nombre + " " + this.voluntario.apellido + " a " + this.beneficiario.nombre + " " + this.beneficiario.apellido ))

      this.beneficiario.asociado.push(this.voluntario);
      this.fs.updateMayor(this.id, this.beneficiario )
              .finally(
                () => this.router.navigate(['/listadobeneficiarios'])

              );


    //this.fs.addVolutarioToBeneficiario(this.beneficiario.id,this.voluntario.id).finally(
      
  

   }

}
