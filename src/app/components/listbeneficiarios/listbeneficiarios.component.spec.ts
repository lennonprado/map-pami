import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListbeneficiariosComponent } from './listbeneficiarios.component';

describe('ListbeneficiariosComponent', () => {
  let component: ListbeneficiariosComponent;
  let fixture: ComponentFixture<ListbeneficiariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListbeneficiariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListbeneficiariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
