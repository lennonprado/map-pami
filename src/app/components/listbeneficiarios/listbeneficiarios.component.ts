import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listbeneficiarios',
  templateUrl: './listbeneficiarios.component.html',
  styleUrls: ['./listbeneficiarios.component.scss']
})
export class ListbeneficiariosComponent implements OnInit {

  mayores : any[] = [];
  
  constructor(private fs: FirebaseService  ) { }

  ngOnInit(): void {
    this.getBeneficiarios();
  }

  getBeneficiarios(){
    return this.fs.getMayores().subscribe(

      response => {
        this.mayores = [];
        response.forEach(doc => { 
          let element = doc.data();
          element.id = doc.id; 
          this.mayores.push(element);          
        });
      },
      error => {console.log(error);
      }
    );
  }

  deleteMayor(item){
    if(confirm("Seguro quiere eliminar a " + item.nombre + " " + item.apellido )) {
      this.fs.deleteMayor(item.id);
    }
    
  }


  tiene(item){
    if(item == undefined)
      return 0;
    else if(item.asociado == undefined)
      return 0
    else if(item.asociado == [])
      return 0
    else if(item.asociado.length > 0)
      return item.asociado.length;
    else  
      return 0;      
  }

}
