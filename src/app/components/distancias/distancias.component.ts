import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { element } from 'protractor';

@Component({
  selector: 'distancias-mayores',
  templateUrl: './distancias.component.html',
  styleUrls: ['./distancias.component.scss']
})



export class DistanciasComponent implements OnInit {

  @Input() ciudad : any;
  @Input() latitude : any;
  @Input() longitude : any;
  @Output() voluntario = new EventEmitter<any>();

  voluntarios : any[] = [];

  constructor(public fs : FirebaseService) { }

  ngOnInit(): void {
    this.getVoluntarios();
  }

  ngOnChanges(){
    this.getDistancias();

  }

  getVoluntarios(){
    console.log(this.ciudad);
    
    return this.fs.getVoluntarioPorCiudad(this.ciudad).subscribe(
      response => {
        this.voluntarios = [];
        response.forEach(doc => { 
          let element = doc.data();
          element.id = doc.id; 
          this.voluntarios.push(element);          
        });
        this.getDistancias();
      },
      error => {console.log(error);
      }
    );
  }

  getDistancias(){
    this.voluntarios.forEach( e =>{
      e.distancia = this.getKilometros(this.latitude,this.longitude,e.latitude,e.longitude);
    });
    this.voluntarios.sort( (a,b) =>  a.distancia - b.distancia  );
  }


  getKilometros(lat1,lon1,lat2,lon2){    
    if((lat1 != undefined) && (lon1 != undefined) && (lat2 != undefined) && (lon2 != undefined)){
      let rad = function(x) {return x*Math.PI/180;}
      let R = 6378.137; //Radio de la tierra en km
      let dLat = rad( lat2 - lat1 );
      let dLong = rad( lon2 - lon1 );
      let a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      let d = (R * c * 1000);
      return d.toFixed(0);
    }
    else{
      return null;
    }
  }

  
  tiene(item){
    if(item == undefined)
      return 0;
    else if(item.asociado == undefined)
      return 0
    else if(item.asociado == [])
      return 0
    else if(item.asociado.length > 0)
      return item.asociado.length;
    else  
      return 0;      
  }

  selected(item){
    
    this.voluntario.emit(item);
  }

}
