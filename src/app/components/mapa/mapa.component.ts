import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';



import * as Leaflet from 'leaflet';
import { element } from 'protractor';

declare const L: any; 

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.scss']
})
export class MapaComponent implements OnInit {

  voluntarios : any[] = [];
  mayores : any[] = [];

  latitude: any = -37.32872186977888;
  longitude: any = -59.1369104424321;
  zoom = 9;
  selected = false;
  submited = false;
  map: any;
  estado: any;
  markers: any[] = [];

  mayorSelected: any;
  voluntarioSelected: any;

  iconVoluntario = L.icon({
    iconUrl: 'assets/img/voluntario.png',
    shadowUrl: 'assets/img/shadow.png',
    iconSize: [35, 50],
    shadowSize:   [50, 50],
    iconAnchor: [18, 50],
    shadowAnchor: [18, 50],
    popupAnchor: [0, -45],
  });

  iconMayor = L.icon({
    iconUrl: 'assets/img/mayor.png',
    shadowUrl: 'assets/img/shadow.png',
    iconSize: [35, 50],
    shadowSize:   [50, 50],
    iconAnchor: [18, 50],
    shadowAnchor: [18, 50],
    popupAnchor: [0, -45],
  });

  mapaXYZ = 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';

  constructor(private fs : FirebaseService) { }

  ngOnInit(): void {
    this.drawMap();
    this.getVoluntarios();
    this.getMayores();
  }

  getVoluntarios(){
    return this.fs.getVoluntarios().subscribe(
      response => {
        response.forEach(doc => {
          let element = doc.data();
          element.id = doc.id; 
          this.voluntarios.push(element);         
          this.addmarkerVoluntario(element);
        })
      },
      error => {console.log(error);
      }
    );
  }

  getMayores(){
    return this.fs.getMayores().subscribe(
      response => {
        response.forEach(doc => {
          let element = doc.data();
          element.id = doc.id; 
          this.mayores.push(element);          
          this.addmarkerMayor(element);
        })
      },
      error => {console.log(error);
      }
    );
  }

  drawMap(): void {
    this.map = Leaflet.map('map').setView([this.latitude, this.longitude], this.zoom);
    Leaflet.tileLayer(this.mapaXYZ, {attribution: 'Extension UNICEN',maxZoom: 17}).addTo(this.map);
    // Add marker on click
    this.map.on('click', (e) =>{  });
  }
  
  addmarkerVoluntario(info){
    let marker = L.marker([info.latitude,info.longitude],{ draggable: false, icon : this.iconVoluntario}).on('click',() => {console.log(info)  }).addTo(this.map).bindPopup(info.nombre);    
    info.marker = marker;
    this.markers.push(marker);  
  }

  addmarkerMayor(info){
    let marker = L.marker([info.latitude,info.longitude],{ draggable: false, icon : this.iconMayor}).on('click',() => {console.log(info)  }).addTo(this.map).bindPopup(info.nombre);    
    info.marker = marker;
    this.markers.push(marker);  
  }

  onmarkclick(e){
    console.log(e);
  }



}
