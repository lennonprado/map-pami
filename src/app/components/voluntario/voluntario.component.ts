import { FirebaseService } from './../../services/firebase.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';



import * as Leaflet from 'leaflet';

declare const L: any; 

@Component({
  selector: 'app-voluntario',
  templateUrl: './voluntario.component.html',
  styleUrls: ['./voluntario.component.scss']
})
export class VoluntarioComponent implements OnInit {

  latitude: any = -37.32872186977888;
  longitude: any = -59.1369104424321;

  zoom = 15;
  selected = false;
  submited = false;
  map: any;
  estado: any;
  public marker: any = undefined;

  myIcon = L.icon({
    iconUrl: 'assets/img/pointer.png',
    shadowUrl: 'assets/img/shadow.png',
    iconSize: [35, 50],
    shadowSize:   [50, 50],
    iconAnchor: [18, 50],
    shadowAnchor: [18, 50],
    popupAnchor: [0, 0],
  });


  mapaXYZ = 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}';
  
  constructor( private fs: FirebaseService) {}
  
  @ViewChild('signupForm') form: NgForm;

  ngOnInit() {
    this.drawMap();
    this.setCurrentLocation();
  }

  onSubmit(f: NgForm) {
    if(this.selected){ 
    f.value.latitude = this.latitude;
    f.value.longitude = this.longitude;
    f.value.asociado = [];
    this.fs.addVoluntario(f.value).then(() => {
      f.reset();
      this.submited = true;
      this.map.removeLayer(this.marker);
    }, (error) => {
      console.error(error);
      
      alert("Hubo un error al guardar los datos en la base de datos, asegurese de tener internet.");
    });

    }
    else{
      alert("Seleccione su localización en el mapa.");
    }
  }

  drawMap(): void {
    this.map = Leaflet.map('map').setView([this.latitude, this.longitude], this.zoom);
    Leaflet.tileLayer(this.mapaXYZ, {attribution: 'Extension UNICEN',maxZoom: 17}).addTo(this.map);
    // Add marker on click
    this.map.on('click', (e) =>{ this.addmarker(e); });
  }
  
  addmarker(e){
    this.selected = true;
    this.latitude = e.latlng.lat;
    this.longitude = e.latlng.lng;
    if(this.marker != undefined) 
    this.map.removeLayer(this.marker);
    let marker = L.marker([this.latitude,this.longitude],{ draggable: true, icon : this.myIcon}).addTo(this.map);    
    this.marker = marker;  
    this.map.setView([e.latlng.lat,e.latlng.lng]);
    console.log("LAT: " + this.latitude + "LON: " + this.latitude );
  }

  addmarker2(lat,lon){
    this.selected = true;
    this.latitude = lat;
    this.longitude = lon;
    if(this.marker != undefined) 
    this.map.removeLayer(this.marker);
    let marker = L.marker([this.latitude,this.longitude],{ draggable: true, icon : this.myIcon}).addTo(this.map);    
    this.marker = marker;      
    this.map.setView([lat,lon]);
    console.log("LAT: " + this.latitude + "LON: " + this.latitude );
  }

  public buscarDireccion(e){

    this.fs.getPuntosGis(this.form.value.direccion + ', ' + this.form.value.ciudad).subscribe(
      data => {
        if(data[0] != undefined ){
          let e;
          this.addmarker2(data[0].lat, data[0].lon);
          
        }
      
      },
      error => { console.log(error)}
    );
    
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.map.setView([this.latitude, this.longitude], this.zoom);        
      });
    }
    
  }
}
