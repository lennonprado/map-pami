import { AsignacionesComponent } from './components/asignaciones/asignaciones.component';
import { ListvoluntariosComponent } from './components/listvoluntarios/listvoluntarios.component';
import { EditmayoresComponent } from './components/editmayores/editmayores.component';
import { MapaComponent } from './components/mapa/mapa.component';
import { EditvoluntarioComponent } from './components/editvoluntario/editvoluntario.component';
import { MayoresComponent } from './components/mayores/mayores.component';
import { LoginComponent } from './components/login/login.component';
import { VoluntarioComponent } from './components/voluntario/voluntario.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListbeneficiariosComponent } from './components/listbeneficiarios/listbeneficiarios.component';
import { ListadoasignadosComponent } from './components/listadoasignados/listadoasignados.component';


const routes: Routes = [
  { path: '', component: VoluntarioComponent },
  { path: 'voluntarios', component: VoluntarioComponent  },
  { path: 'login', component: LoginComponent  },
  { path: 'mapa', component: MapaComponent  },
  { path: 'altabeneficiario', component: MayoresComponent  },
  { path: 'listadovoluntarios', component: ListvoluntariosComponent  },
  { path: 'listadobeneficiarios', component: ListbeneficiariosComponent  },
  { path: 'editvoluntario/:id', component: EditvoluntarioComponent  },
  { path: 'editmayores/:id', component: EditmayoresComponent  },
  { path: 'asignaciones', component: AsignacionesComponent  },
  { path: 'asignaciones/:id', component: AsignacionesComponent  },
  { path: 'asignados', component: ListadoasignadosComponent  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
