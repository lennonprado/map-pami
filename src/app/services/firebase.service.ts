import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  user:any;
  ciudad:any;

  constructor(private http: HttpClient, private firestore: AngularFirestore) { }


  getPuntosGis(direccion){
    return this.http.get('https://nominatim.openstreetmap.org/search?format=json&q='+direccion+',Argentina');
  }

  //Crea un nuevo voluntario
  public addVoluntario(data) {
    return this.firestore.collection('voluntarios').add(data);
  }

  //Crea un nuevo adulto mayor
  public addMayor(data) {
    return this.firestore.collection('mayores').add(data);
  }

  public getBeneficiario(documentId: string) {
    return this.firestore.collection('mayores').doc(documentId).snapshotChanges();
  }
  
  public getVoluntario(documentId: string) {
    return this.firestore.collection('voluntarios').doc(documentId).snapshotChanges();
  }

  public getCiudad(adminEmail: string) {
    return this.firestore.collection('administradores').doc(adminEmail).snapshotChanges();
  }

  public getMayores(){
    //return this.firestore.collection('mayores').snapshotChanges();
    return this.firestore.collection('mayores', ref => ref.where("ciudad", "==", this.getCity() )).get();
  }

  public getVoluntarios(){
    //return this.firestore.collection('voluntarios').snapshotChanges();
    return this.firestore.collection('voluntarios', ref => ref.where("ciudad", "==", this.getCity())).get();

  }

  public updateVoluntario(documentId: string, data: any) {
    return this.firestore.collection('voluntarios').doc(documentId).set(data);
  }

  public updateMayor(documentId: string, data: any) {
    return this.firestore.collection('mayores').doc(documentId).set(data);
  }
  
  public deleteVoluntario(id){    
      return this.firestore.collection("voluntarios").doc(id).delete();
  }

  public deleteMayor(id){    
    return this.firestore.collection("mayores").doc(id).delete();
  }

  public getVoluntarioPorCiudad(ciudad){
    return this.firestore.collection('voluntarios', ref => ref.where("ciudad", "==", ciudad)).get();
  }

  public addVolutarioToBeneficiario(idBeneficiario,idVoluntario){

   // return this.firestore.collection("asignaciones").doc(idBeneficiario).set({'voluntario' : idVoluntario})
  
  
  }

  public setCity(city){
    console.log(city);
    
    this.ciudad = city.ciudad;
    localStorage.setItem('ciudad', JSON.stringify(city.ciudad));    
  }
  public getCity(){    
    if(this.ciudad == undefined)
      this.ciudad = JSON.parse(localStorage.getItem('ciudad'));   
    return this.ciudad;
  }

  public setUser(user){
    this.user = user;
    localStorage.setItem('user', JSON.stringify(this.user));    
  }

  public getUser(){    
    if(this.user == undefined)
      this.user = JSON.parse(localStorage.getItem('user'));   
    return this.user;
  }

  public loggedIn(){
    let user = this.getUser();
    if(this.user == undefined)
      return false;
    else  
      return true;  
  }

  async logout(){
    localStorage.removeItem('user');
    localStorage.removeItem('ciudad');

  }


 
  /*
  public getTodos(): Observable<firebase.firestore.QuerySnapshot> {
    return this.firestore.collection('voluntarios', ref => 
     ref.where("userId", "==", userId).orderBy('lastModifiedDate', 'desc'))
   .get();
  }
  */



  /*
  //Obtiene un gato
  public getCat(documentId: string) {
    return this.firestore.collection('cats').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los gatos
  public getCats() {
    return this.firestore.collection('cats').snapshotChanges();
  }
  //Actualiza un gato
  public updateCat(documentId: string, data: any) {
    return this.firestore.collection('cats').doc(documentId).set(data);
  }
*/

}
